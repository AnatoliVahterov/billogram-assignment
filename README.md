## Running

To run the app just run the command below from the root of repo

```
docker-compose up --build
```

You can access Swagger `http://localhost:5055/apidocs/`.


# Feature Design

| Campaign       |
| ---------------|
| id             | 
| brand_id       |
| discount_type  | 
| discount_value | 
| quantity_total | 
| quantity_used  | 
| valid_from     | 
| valid_to       | 
| created_at     | 

one-to-many relationship

| Discount Code  |
| ---------------|
| id             | 
| user_id        |
| campaign_id    | 
| code           | 
| acctive        | 
| created_at     | 

create campaign:
Brand creates a campaign by specifying discount type, discount amount(value), total number of discount codes to be available and dates when discount code should be valid.
Discount type can be either a fixed discount e.g -100kr or a percentage discount e.g -20%.


get discount code:
The 'get discount code' endpoint will create and return a discount code within brand's campaign if the limit of issued discount codes is not reached (campaign.quantity_used < campaign.quantity_total).


A code can be 6 character long string of random digits and letters, should be unique within its campaign.
