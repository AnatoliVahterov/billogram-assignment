from flasgger import Swagger

import api

app = api.create_app()

app.config['SWAGGER'] = {
    'title': 'Discount code API',
    'uiversion': 2
}
Swagger(app, template_file='api.yaml')
import api.routes

if __name__ == "__main__":
    app.run(host='0.0.0.0')
