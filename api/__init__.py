from flask import Flask
import os
import time
from api.models import db

def create_app():

    app = Flask(__name__)

    app.config['SQLALCHEMY_DATABASE_URI'] = os.getenv('SQLALCHEMY_DATABASE_URI')
    app.config['SQLALCHEMY_TRACK_MODIFICATIONS'] = False

    from api.models.campaign import Campaign
    from api.models.discountcode import DiscountCode
    db.init_app(app)
    
    with app.app_context():
        dbstatus = False
        while dbstatus == False:
            try:
                db.create_all()
            except:
                print('waiting for db...')
                time.sleep(2)
            else:
                dbstatus = True
    return app