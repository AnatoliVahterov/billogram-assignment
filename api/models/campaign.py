import uuid

from sqlalchemy.dialects.postgresql import UUID
from sqlalchemy.sql import func

from . import db


class Campaign(db.Model):
  
    id = db.Column(UUID(as_uuid=True), primary_key=True, default=uuid.uuid4)
    brand_id = db.Column(UUID(as_uuid=True), nullable=False)
    discount_type = db.Column(db.String(32))
    discount_value = db.Column(db.String(32))
    quantity_total = db.Column(db.Integer) 
    quantity_used = db.Column(db.Integer, default=0) 
    valid_from = db.Column(db.DateTime)
    valid_to = db.Column(db.DateTime)
    created_at = db.Column(db.DateTime(timezone=True), server_default=func.now())

    def __init__(self, brand_id, discount_type, discount_value, quantity_total, valid_from, valid_to):
        self.brand_id = brand_id
        self.discount_type = discount_type
        self.discount_value = discount_value
        self.quantity_total = quantity_total
        self.valid_from = valid_from
        self.valid_to = valid_to
        
    @property
    def serialized(self):
        """Return object data in serializable format"""
        return {
            'id': self.id,
            'brand_id': self.brand_id,
            'discount_type': self.discount_type,
            'discount_value': self.discount_value,
            'quantity_total': self.quantity_total,
            'quantity_user': self.quantity_used,
            'valid_from': self.valid_from,
            'valid_to': self.valid_to,
            'created_at': self.created_at,
        }
