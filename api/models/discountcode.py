import uuid

from sqlalchemy.dialects.postgresql import UUID
from sqlalchemy.sql import func

from api.models.campaign import Campaign
from . import db


class DiscountCode(db.Model):
  
    id = db.Column(UUID(as_uuid=True), primary_key=True, default=uuid.uuid4)
    user_id = db.Column(UUID(as_uuid=True), nullable=False)
    campaign_id = db.Column(db.ForeignKey(Campaign.id), nullable=False)
    code = db.Column(db.String, default=lambda: uuid.uuid4().hex[:6].upper(), unique=True)
    active = db.Column(db.Boolean, nullable=True, default=True)
    created_at = db.Column(db.DateTime(timezone=True), server_default=func.now())

    def __init__(self, user_id, campaign_id):
        self.user_id = user_id
        self.campaign_id = campaign_id
        
    @property
    def serialized(self):
        """Return object data in serializable format"""
        return {
            'id': self.id,
            'user_id': self.user_id,
            'campaign_id': self.campaign_id,
            'code': self.code,
            'active': self.active,
            'created_at': self.created_at,
        }
