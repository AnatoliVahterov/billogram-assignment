import flask
import sqlalchemy.orm.exc
from __main__ import app

import api.models.campaign
import api.models.discountcode
import api.validation
from . import db


@app.route('/v1/campaign', methods=['POST'])
def create_campaign():
    """   
    """
    data = flask.request.get_json()
    required_columns = [
        'brand_id',
        'discount_type',
        'discount_value',
        'quantity_total',
        'valid_from',
        'valid_to',
    ]
    api.validation.check_required_columns(data, required_columns)

    record = api.models.campaign.Campaign(**data)
    db.session.add(record)
    try:
        db.session.commit()
    except sqlalchemy.exc.SQLAlchemyError:
        db.session.rollback()
        return {'error': 'DB error'}, 500
    return flask.jsonify({'data': record.serialized}), 200


@app.route('/v1/discount-code', methods=['POST'])
def create_discount_code():
    """
    """
    data = flask.request.get_json()
    required_columns = [
        'user_id',
        'campaign_id',
    ]
    api.validation.check_required_columns(data, required_columns)

    try:
        campaign = api.models.campaign.Campaign.query.get(data['campaign_id'])
    except sqlalchemy.exc.SQLAlchemyError:
        return {'error': 'Invalid campaign_id'}, 400

    if campaign and campaign.quantity_used < campaign.quantity_total:
        discount_code = api.models.discountcode.DiscountCode(**data)
        campaign.quantity_used += 1
        db.session.add(discount_code)
        try:
            db.session.commit()
        except sqlalchemy.exc.SQLAlchemyError:
            db.session.rollback()
            return {'error': 'DB error'}, 500
        return flask.jsonify({'data': discount_code.serialized}), 200

    return {'data': 'No discount codes available for this campaign'}, 200
